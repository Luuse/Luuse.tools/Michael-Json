% rebase('templates/base.tpl')
<section data-project="{{ project }}" class="header">
	<div class="btns">
	</div>
	<input class="btn_json_add" type="button" value="+" />
	<div class="gestion-projects">
		<input class="btn_run" type="button" value="RUN !! " />
		<input id="upload_file" type="file" value="upload Model"> | 
	 project > [  
	<select id="projects" onchange="location = this.value;">
		% for p in list_projects:
			% if p == project:
				<option value="/{{ p }}" selected>{{ p }}</option>
			% else: 
				<option value="/{{ p }}" >{{ p }}</option>
			% end 
		% end
	</select>
		]
		<input id="new_project" type="button" value="New Project">
		<input id="new_project" type="text" value="project name">
	</div>
</section>

<section class="workspace">
	<section class="editors">
	</section>
	<section class="preview">
		<div class="inter_box">
			<div class="inter_adjust">
				<canvas id="canvas_output" class="img_output" ></canvas>
				<div class="event_log"></div>
			</div>
		</div>
	</section>
		<section class="model">
			<div class="image">
				<div class="inter_box">
					<div class="inter_adjust">
						<canvas id="canvas_input" class="img_input"></canvas>
					</div>
				</div>
				<div class="info_model"></div>
			</div>
			<div class="nav_model">
				<form id="upload_model" enctype="multipart/form-data" method="post" name="fileinfo">
					<input type="file" name="file_model" required />
					<input type="submit" value="send file" />
				</form>
			</div>
		</section>
	</section></section>
	</section>
