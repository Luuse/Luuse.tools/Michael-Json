#!/usr/bin/python
# -*- coding: utf-8 -*-

from bottle import Bottle, run, template, route, static_file, get, request
import os, glob, subprocess
from michaeljson import MichaelJson, Utility

app = Bottle()
ROOT_PATH = os.path.dirname(os.path.abspath(__file__))

@app.route('/static/<filepath:path>')
def asset(filepath):
    return static_file(filepath, root=os.path.join(ROOT_PATH, 'static'))

@app.route('/files/<filepath:path>')
def files(filepath):
    return static_file(filepath, root=os.path.join(ROOT_PATH, 'files'))

@app.route('/')
@app.route('/<project>')
def index(project=False):
    dp = glob.glob('files/*/')
    list_projects = [p.split('/')[-2] for p in dp]

    return template('templates/index.tpl', list_projects=list_projects, project=project, outputsvg='files/'+project+'/output/output.svg')


@app.route('/run/<project>', method='GET')
def run_project(project): 
    dj = glob.glob('files/'+project+'/json/*.json')
    im = glob.glob('files/'+project+'/input/*.*')
    opts = []
    for j in dj: opts.append(Utility.read_json(j))
    MJ = MichaelJson()
    out = MJ.compose_svg(im[0], opts)
    out.saveas('files/'+project+'/output/output.svg')
    return  project 


@app.route('/new_project/<project>', method='GET')
def new_project(project): 
    return project 

@app.route('/write_json', method='post')
def write_json():
    PROJECT = request.forms.project
    namejson= request.forms.name
    json = request.forms.json

    file = open('files/'+PROJECT+'/json/'+namejson+'.json','w')
    file.write(json)
    file.close()

    return 'json write !'

@app.route('/delete_json', method='post')
def write_json():
    PROJECT = request.forms.project
    namejson= request.forms.name

    os.remove('files/'+PROJECT+'/json/'+namejson+'.json')

    return 'json delete!'


@app.route('/get_jsons', method='post')
def get_jsons():
    PROJECT = request.forms.project
    dir_jsons = glob.glob('files/'+PROJECT+'/json/*')
    list_jsons = []
    for j in dir_jsons:
        if j.split('.')[-1] in ['json']:
            name_json = j.split('/')[-1]
            name_json = name_json.split('.')[0]
            list_jsons.append(name_json)
    list_jsons.sort()

    return '||'.join(list_jsons)

@app.route('/upload_model/<project>', method='post')
def upload_model(project):
    data = request.files.file_model 
    name, ext = os.path.splitext(data.filename)
    raw = data.file.read()
    dir_in = 'files/'+project+'/input/' 
    path = dir_in + 'input'+ ext
    file = open(path, 'wb')
    file.write(raw)
    file.close

    for fi in glob.glob(dir_in + '*'):
        if fi != path:
            os.remove(fi)
        else:
            info = MichaelJson().get_info(path)

    return path+'|'+str(info['width'])+'|'+str(info['height'])


@app.route('/load_model/<project>', method='GET')
def load_model(project):
    path = glob.glob('files/'+project+'/input/*')[0]
    info = MichaelJson().get_info(path)
    return path+'|'+str(info['width'])+'|'+str(info['height'])

run(app, host="0.0.0.0", port=8080, reloader=True, debug=True)
